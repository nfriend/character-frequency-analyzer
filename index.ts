import chalk from 'chalk';
import * as glob from 'glob';
import { argv } from 'yargs';
const fs = require('fs');
import * as bluebird from 'bluebird';

const globAsync = bluebird.promisify<string[], string>(<any>glob);
bluebird.promisifyAll(fs);

(async () => {
  // maps each character to its count
  const characterCount = {};

  const files = await globAsync(<any>argv.pattern);

  files.map(f => {
    console.log(chalk.gray(`Scanning ${f}...`));

    let data = '';
    try {
      data = fs.readFileSync(f, 'utf8');
    } catch (err) {
      // ignore the error
      console.log(chalk.red(`Error: ${err}`));
    }

    for (let i = 0; i < data.length; i++) {
      const char = data[i].toLowerCase();

      // ignore characters we don't care about
      if (char === '\n' || char === ' ' || /[a-z]/i.test(char)) {
        continue;
      }

      if (!characterCount[char]) {
        characterCount[char] = 0;
      }

      characterCount[char]++;
    }
  });

  console.log(`${chalk.blueBright('Character')}: ${chalk.green('Frequency')}`);
  console.log(`--------------------`);

  Object.keys(characterCount)
    .map(char => {
      return {
        char,
        count: characterCount[char],
      };
    })
    .sort((a, b) => {
      return a.count - b.count;
    })
    .forEach(pair => {
      console.log(`${chalk.blueBright(pair.char)}: ${chalk.green(pair.count)}`);
    });
})();
