# Character Frequency Analyzer

A command-line utility that scans a group of files and reports the frequency of characters in those files.

This analyzer is _very_ rudimentary - its only purpose is to help me analyze some JavaScript to inform my custom keyboard layout!

## Usage

`npm run start -- --pattern "**/*.js"`
